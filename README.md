# Simple Docker-Nginx loadbalancer

The simplest way how to create working load balancer based on Docker and Nginx

Data in [webserver](webserver) folder represents web application

## Prerequisites
It assumes that you have installed these tools:
- [Git](https://git-scm.com/downloads)
- [Docker](https://docs.docker.com/install/)
- [Docker-compose](https://docs.docker.com/compose/install/)

## Usage
Simply clone repository to you filesystem and run `docker-compose`.

**NOTE**: This example HAVE TO be cloned to `simple-docker-nginx-loadbalancer` folder to work properly. Web server hostnames are hardcoded in [loadbalancer configuration](loadbalancer/default.conf). On production you should always use some dynamic configuration service which will represent the current state of load balanced service (when you create new service instance, terminate some instance, etc.)

```
git clone git@gitlab.com:hmlkao-examples/simple-docker-nginx-loadbalancer.git
cd simple-docker-nginx-loadbalancer
docker-compose up --scale web=2
```

**NOTE**: You can ran more then 2 web server services but it will have no effect, on the other hand you HAVE TO run at least 2 web server services, loadbalancer is set up exactly for 2 servers.

When you open http://localhost page in your web browser you will see that the requests are sent to both web servers in log of `docker-compose` command.

## Cleanup
After you are satisfied with the test you should run this command (in different terminal but in the same folder) to clean up.
```
cd simple-docker-nginx-loadbalancer
docker-compose down
```
